from .task import Task
from .user import User
from .logger import Logger
from .priority import PRIORITIES
from solution.status import COMPLETED
from datetime import datetime, timezone
from .project import Project
from dateutil.relativedelta import *
from dateutil.parser import *
from solution.storage import Storage

"""
    the base program module
"""


class Manager:
    """
        the base program module
    """

    def __init__(self):
        self.current_user = None
        self.current_project = None
        self.users = {}
        self.projects = {}
        self.storage = Storage()

    """
        Methods to work with users
    """

    def check_finish(self):
        if self.current_user:
            return self.users[self.current_user].check_finish()

    def add_user(self, login):
        if login:
            user = User(login)
            self.users[user.id] = user
            self.current_user = str(user.id)
            Logger.get_logger().info('Add user {0} with id:{1}'.format(user.login, user.id))
        else:
            raise NameError('login is none')

    def delete_user(self, _id):
        if _id in self.users:
            del self.users[_id]
            Logger.get_logger().info('Delete user:{0}'.format(_id))
        else:
            Logger.get_logger().error('User with id:{0} not found!'.format(_id))
            raise NameError('Wrong id! user not delete')

    def chose_current_user(self, _id):
        if _id in self.users:
            self.current_user = _id
            Logger.get_logger().info('Chose current user:{0}'.format(self.users[self.current_user].login))
        else:
            Logger.get_logger().error('User with id:{0} not found!'.format(_id))
            raise NameError('Wrong id!')

    def print_current_user(self):
        if self.current_user:
            Logger.get_logger().info('Print current user')
            user = self.users[self.current_user]
            return user
        else:
            Logger.get_logger().error('no current user')
            raise NameError('Current user is None')

    def print_all_users(self):
        if not self.users:
            Logger.get_logger().error('no users')
            raise NameError('No users')
        Logger.get_logger().info('Print all users')
        return self.users

    def print_user_tag(self):
        if self.current_user:
            tags = self.users[self.current_user].print_tags()
            if tags:
                Logger.get_logger().info('Print user tags')
                return tags
            else:
                Logger.get_logger().error('Print user tags(user dont have tags)')
        else:
            Logger.get_logger().error('no current user')
            raise NameError('Current user is None')

    """
        Methods to work with tasks
    """

    def add_task(self, name=None, priority=1, finish=None, description=None,
                 deadline=None, period=None, id=None, user=None, parent=None, tags=None, project=None):
        """
            Function to make a task
            :param name: task name
            :param priority: task priority
            :param finish: task finish
            :param description: task description
            :param deadline: task deadline
            :param period: task period
            :return: task
        """
        if not deadline and period:
            Logger.get_logger().error('Cannot add task because task is periodic and no deadline.')
            raise NameError('if task periodic, you must input deadline')

        if int(priority) > 2 or int(priority) < 0:
            Logger.get_logger().error('Priority {0} doesn\'t exist'.format(priority))
            raise NameError('Wrong priority!')

        parsed_period = self.get_time_period(period)
        print(type(parsed_period))
        task = Task(user=user,
                    id=id,
                    name=name,
                    priority=priority,
                    finish=finish,
                    description=description,
                    deadline=deadline,
                    period=period,
                    parent=parent,
                    tags=tags,
                    project=project)

        # if parent:
        #     parent_task = self.storage.get_task(parent)
        #     if parent_task:
        #         parent_task.subtasks.append(task.key)
        #         task.parent = parent_task.key
        #         task.host = parent_task.host
        #         task.admins = parent_task.admins.copy()

        return task

    def add_user_task(self, name=None, priority=1, finish=None, description=None, project=None,
                      deadline=None, parent=None, id=None, user=None, periodicity=None, tags=None):
        """
            Function to add a task to user
            :param name: task name
            :param priority: task priority
            :param finish: task finish
            :param description: task description
            :param deadline: task deadline
            :param period: task period
            :return: None
        """
        task = self.add_task(name=name, priority=priority, finish=finish, description=description, project=project,
                             deadline=deadline, period=periodicity, id=id, user=user, parent=parent, tags=tags)
        self.storage.save_task(task)
        #Logger.get_logger().info('Task (id:{0}) added to user {1}'.format(task.id, self.users[self.current_user].login))

    def add_task_to_storage(self, task):
        if task.parent:
            parent_task = self.storage.get_task(task.parent)
            if parent_task:
                parent_task.subtasks.append(task.key)
                task.parent = parent_task.key
                task.host = parent_task.host
                task.admins = parent_task.admins.copy()
                for admin in task.admins:
                    self.storage.save_message(admin, "Admin {0} was added to task N{1}".format(admin, task.key))
                Logger.get_logger().debug("Task N{0} was added how subtask to task N{1}".format(task.key,
                                                                                                parent_task.key))
                self.__storage.save_task(parent_task)

        self.storage.save_task(task)

    # def user_add_subtask(self, _id, name=None, priority=1, finish=None, description=None,
    #                      period=None, deadline=None):
    #     """
    #         Function to add a a subtask
    #         :param name: task name
    #         :param priority: task priority
    #         :param finish: task finish
    #         :param description: task description
    #         :param deadline: task deadline
    #         :param period: task period
    #         :return: None
    #     """
    #     if not self.current_user:
    #         Logger.get_logger().error('Cannot add subtask because user not authorized.')
    #         raise NameError('You haven\'t authorized')
    #     task = self.add_task(name=name, priority=priority, finish=finish, description=description,
    #                          deadline=deadline, period=period)
    #     self.users[self.current_user].add_subtask(_id, task)
    #     Logger.get_logger().info('SubTask (id:{0}) added to task (id:{1})  (user {2})'
    #                              .format(task.id, _id, self.users[self.current_user].login))

    def user_change_task(self, _id, name=None, status=None, priority=None, finish=None, description=None,
                         deadline=None, period=None):
        """
            Function to change a task
            :param _id: task id of task to be change
            :param name: task new name
            :param status: task new status
            :param priority: task new priority
            :param finish: task new finish
            :param description: task new description
            :param deadline: task new deadline
            :param period: task new period
            :return: None
        """
        task = self.add_task(name=name, priority=priority, finish=finish, description=description,
                             deadline=deadline, period=period, id=id)
        changed_task = self.storage.get_task(_id)
        changed_task.delete()
        task.id = _id

        self.storage.save_task(task)

        Logger.get_logger().info('Task (id:{0}) changed'.format(_id))

    def user_delete_task(self, _id):
        """
            Delete a task with all its subtasks
            :param _id: task id of task to be removed
            :return: None
        """
        try:
            self.storage.get_task(_id)
            Logger.get_logger().info('Task (id:{0}) delete'.format(_id))
        except NameError as e:
            Logger.get_logger().error('Task (id:{0}) dont delete({1})'.format(_id, e))
            raise NameError(e)

    # def complete_user_task(self, _id):
    #     """
    #         Function to complete a task.
    #         :param _id: id task to complete
    #         :return: None
    #     """
    #     task = self.storage.get_task(_id)
    #     print(type(task))
    #     task = self.complete_task(task)
    #     self.storage.save_task(task)
    #     # Logger.get_logger().info('Task (id:{0}) is completed  (user {1})'.format(_id, self.users[self.current_user].login))
    #
    # @staticmethod
    # def complete_task(task):
    #     print(type(task))
    #     task.status = COMPLETED
    #     return task

    def complete_user_task(self, _id):
        """
            Function to complete a task.
            :param _id: id task to complete
            :return: None
        """
        task = self.storage.get_task(_id)
        print(task.name)
        task = self.complete_task(task)
        self.storage.save_task(task)
        # Logger.get_logger().info('Task (id:{0}) is completed  (user {1})'.format(_id, self.users[self.current_user].login))

    def complete_task(self, task):
        if task.period:
            new_task = Manager.start_task(task)
            task.status = COMPLETED
            self.storage.save_task(task)
            return new_task
        task.status = COMPLETED
        return task

    @staticmethod
    def start_task(task):
        """
            For periodic task.
            :param _id: id task to start
            :return: None
        """

        if datetime.now(timezone.utc) < task.finish:
            if task.deadline > task.finish:
                task.status = FAIL
            else:
                new_task = Task(user=task.user, name=task.name, description=task.description, finish=task.finish,
                                deadline=task.deadline, period=task.period, priority=1,
                                parent=task.parent)
                parsed_period = Manager.get_time_period(task.period)
                print(type(parsed_period))
                print(parsed_period)
                print(task.period)

                new_task.deadline = task.deadline + parsed_period
                while task.deadline < datetime.now(timezone.utc):
                    new_task.deadline = task.deadline + parsed_period
                    new_task.status = IN_PROGRESS
                    print('QQQQQQQQ')
                print(new_task.deadline)
                return new_task

    def user_add_task_tag(self, _id, tag):
        """
            Function to add tags.
            :param _id: id task to add tag
            :param tag: new task tag
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot add task tag because user not authorized.')
            raise NameError('You haven\'t authorized')
        self.users[self.current_user].add_task_tag(tag, _id)
        Logger.get_logger().info('Add tag (#{0}) to Task (id:{1}) (user {2})'
                                 .format(tag, _id, self.users[self.current_user].login))

    def user_delete_task_tag(self, _id, tag):
        """
            Function to delete tags.
            :param _id: id task to delete tag
            :param tag: name removed tag
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot delete task tag because user not authorized.')
            raise NameError('You haven\'t authorized')
        self.users[self.current_user].delete_task_tag(tag, _id)
        Logger.get_logger().info('Delete tag (#{0}) from Task (id:{1}) (user {2})'
                                 .format(tag, _id, self.users[self.current_user].login))

    def user_print_tasks(self):
        """
            Function to give tasks
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise NameError('You haven\'t authorized')
        Logger.get_logger().info('Print all Tasks (user {0})'
                                 .format(self.users[self.current_user].login))
        tasks = self.users[self.current_user].print_tasks()
        return tasks

    def user_print_tasks_by_tag(self, tag):
        """
            Function to give tasks by tag
            :param tag: tag to print tasks
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise NameError('You haven\'t authorized')
        tasks = self.users[self.current_user].print_tasks_by_tag(tag)
        if tasks:
            Logger.get_logger().info('Print Tasks by tag (#{0}) (user {1})'
                                     .format(tag, self.users[self.current_user].login))
            return tasks
        else:
            Logger.get_logger().error('dont Print Tasks by tag (#{0}) (user {1})'
                                      .format(tag, self.users[self.current_user].login))
            raise NameError("tag {0} dont have tasks".format(tag))

    def user_print_archived_tasks(self):
        """
            Function to give archived tasks
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise NameError('You haven\'t authorized')
        tasks = self.users[self.current_user].print_archived_tasks()
        if tasks:
            Logger.get_logger().info('Print archived Tasks (user {0})'
                                     .format(self.users[self.current_user].login))
            return tasks
        else:
            Logger.get_logger().error('dont Print Tasks (user {0})'
                                      .format(self.users[self.current_user].login))
            raise NameError("dont have archived tasks")

    @staticmethod
    def get_time_period(period):
        """
        Make period to datetime format
        :param period:
        :return period in datetime format:
        """
        periods = {"h": relativedelta(hours=1),
                   "d": relativedelta(days=1),
                   "w": relativedelta(days=7),
                   "m": relativedelta(months=1),
                   "y": relativedelta(years=1)}
        return periods.get(period, None)

    """
        Methods to work with projects
    """

    def add_project(self, name):
        """
            Function to make a project
            :param name: task name
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise NameError('You haven\'t authorized')
        project = Project(name)
        project.add_user(self.current_user)
        self.current_project = str(project.id)
        self.projects[project.id] = project
        self.users[self.current_user].add_project(project)
        Logger.get_logger().info('Add new project (name:{0}) (id:{1})'.format(project.name, project.id))

    def print_projects(self):
        """
            Function to print projects
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise NameError('You haven\'t authorized')
        projects = self.users[self.current_user].get_projects()
        if projects:
            Logger.get_logger().info('Print all projects')
            return self.projects
        else:
            raise NameError("You dont have projects")

    def chose_current_project(self, _id):
        """
            Function to chose current project
            :param _id: project id
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise NameError('You haven\'t authorized')
        if _id in self.projects:
            self.current_project = _id
            Logger.get_logger().info('Chose current project: (name:{0}) (id:{1})'
                                     .format(self.projects[self.current_project].name, _id))
        else:
            Logger.get_logger().error('project with id:{0} not found!'.format(_id))
            raise NameError('Wrong id!')

    def project_add_user(self, login):
        """
            Function to add new user in project
            :param login: new user login
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('You haven\'t authorized')
            raise NameError('You haven\'t authorized')
        if not self.current_project:
            Logger.get_logger().error('You haven\'t authorize project')
            raise NameError('You haven\'t authorize project')
        user = User(login)
        user.tasks.tasks = self.projects[self.current_project].print_tasks()
        self.users[user.id] = user
        self.projects[self.current_project].add_user(user.id)
        Logger.get_logger().info('Project (name:{0}) (id:{1}) add user (login:{2}) (id:{3}) '
                                 .format(self.projects[self.current_project].name, self.current_project, user.login,
                                         user.id))
        Logger.get_logger().info('Add user {0} with id:{1}'.format(user.login, user.id))

    def project_add_available_user(self, _id):
        """
           Function to add available user in project
           :param _id: user id
           :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot add because user not authorized.')
            raise NameError('You haven\'t authorized')
        if _id in self.users:
            self.projects[self.current_project].add_user(_id)
            Logger.get_logger().info('Project (name:{0}) (id:{1}) add user (login:{2}) (id:{3}) '
                                     .format(self.projects[self.current_project].name,
                                             self.projects[self.current_project].id,
                                             self.users[_id].login, self.users[_id].id))
        else:
            Logger.get_logger().error('dont Add user with id:{0}'.format(_id))
            raise NameError('Wrong id! dont search user with id')

    def project_delete_user(self, _id):
        """
            Function to delete user from project
            :param _id: user id
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot delete because user not authorized.')
            raise NameError('You haven\'t authorized')
        if self.projects[self.current_project].create_user.id == self.current_user:
            if _id in self.projects[self.current_project].users:
                self.projects[self.current_project].delete_user(_id)
                Logger.get_logger().info('delete user with id:{0}'.format(_id))
            else:
                Logger.get_logger().error('dont delete user with id:{0}'.format(_id))
                raise NameError('Wrong id! dont delete user with id')
        else:
            raise NameError('You don have access')

    def project_get_users(self):
        """
            Function get current project users
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print users because user not authorized.')
            raise NameError('You haven\'t authorized')
        id_array = self.projects[self.current_project].users
        users = {}
        for _id in id_array:
            users[_id] = self.users[str(_id)]
        if users:
            Logger.get_logger().error('print project users')
            return users
        else:
            Logger.get_logger().error('dont have users')
            raise NameError('project dont have users')

    def project_add_task(self, name=None, priority=1, finish=None, description=None,
                         deadline=None, period=None):
        """
            Function to add a task to project
            :param name: task name
            :param priority: task priority
            :param finish: task finish
            :param description: task description
            :param deadline: task deadline
            :param period: task period
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot add task because user not authorized.')
            raise NameError('You haven\'t authorized')
        if self.current_user in self.projects[self.current_project].users:
            task = self.add_task(name=name, priority=priority, finish=finish, description=description,
                                 deadline=deadline, period=period)
        else:
            raise NameError(" You dont have access")
        self.projects[self.current_project].add_task(task, self.users)
        # self.users[self.current_user].add_task(task)
        Logger.get_logger().info('Task (id:{0}) added to priject {1}'.format(task.id, self.current_project))

    def project_add_available_task(self, _id):
        """
            Function to add a task to project
            :param _id: task id
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise NameError('You haven\'t authorized')
        self.projects[self.current_project].add_task(self.users[self.current_user].tasks.get_task(_id), self.users)
        Logger.get_logger().info('Task (id:{0}) added to project (id:{1})'
                                 .format(_id, self.projects[self.current_project].id))

    def project_delete_task(self, _id):
        """
           Function to delete task from project
           :param _id: task id
           :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise NameError('You haven\'t authorized')
        self.projects[self.current_project].delete_task(_id)
        Logger.get_logger().info('Task (id:{0}) delete from project (id:{1})'
                                 .format(_id, self.projects[self.current_project].id))

    def project_print_tasks(self):
        """
            Function to add a task to project
            :return: tasks
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise NameError('You haven\'t authorized')
        return self.projects[self.current_project].print_tasks()

    # def project_add_subtask(self, _id, name=None, priority=1, finish=None, description=None,
    #                         period=None, deadline=None):
    #     if not self.current_user:
    #         Logger.get_logger().error('Cannot add subtask because user not authorized.')
    #         raise NameError('You haven\'t authorized')
    #     task = self.add_task(name=name, priority=priority, finish=finish, description=description,
    #                          deadline=deadline, period=period)
    #     self.projects[self.current_project].add_task(task)
    #     self.users[self.current_user].add_subtask(_id, task)
    #     Logger.get_logger().info('SubTask (id:{0}) added to task (id:{1})  (user {2})'
    #                              .format(task.id, _id, self.users[self.current_user].login))

    # def project_add_subtask(self, _id, name=None, status=0, priority=1, start=None, finish=None, description=None):
    #     task = Task(name, int(status), int(priority), start, finish, description)
    #     self.projects[self.current_project].add_subtask(_id, task)
    #     Logger.get_logger().info('SubTask (id:{0}) added to task (id:{1}) in project (id:{2})'
    #                              .format(task.id, _id, self.projects[self.current_project].login))

    # def project_change_task(self, _id, name=None, status=None, priority=None, start=None, finish=None,
    #                         description=None):
    #     self.projects[self.current_project].change_task(_id, name, status, priority, start, finish, description)
    #     Logger.get_logger().info()
    #
    # def project_complete_task(self, _id):
    #     self.projects[self.current_project].complete_task(_id)
    #

    # def project_print_tasks_by_tag(self, tag):
    #     self.projects[self.current_project].print_tasks_by_tag(tag)
    #
    # def project_print_archived_tasks(self):
    #     self.projects[self.current_project].print_archived_tasks()
