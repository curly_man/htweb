import datetime
from .alltasks import AllTasks
from .status import IN_PROGRESS
from .priority import PRIORITIES


class Task:
    """
            Class that just stores single Task attributes
            name - a name of task
            subtask - child tasks connected to a task.
            parent - id of parent task
            tags - user defined key_words
            start - when the task starts
            finish - when the task ends
            status - one of fixed statuses
            priorities- one of fixed in priorities
            id - unique id of a task
            period - the interval for which the task is postponed
            deadline - used for periodic task
            description - some description of the task
            archived - used for completed task task
    """

    def __init__(self, user=None, name=None, status=IN_PROGRESS, priority=1, finish=None, project=None,
                 description=None, period=None, deadline=None, parent=None, id=None, tags=None):
        self.user = user
        self.name = name
        self.status = status
        self.priority = PRIORITIES[int(priority)]
        self.start = datetime.datetime.now()
        self.period = period
        self.deadline = deadline
        self.finish = finish
        self.description = description
        self.parent = parent
        if id:
            self.id = id
        else:
            self.id = hash(self)
        self.tags = tags
        self.project = project
        self.subtask = AllTasks()
        self.archived = False

