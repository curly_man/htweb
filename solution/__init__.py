"""
Tracker can:

 Work with tasks

1. Add task

2. Add subtask

3. Changed task

4. Complete tasks

5. Delete task

6. Work with periodic tasks

7. Add tags

8. Delete tags

 Work with projects

1. Add project

2. Add user to project(new/available)

3. Delete user from project

4. Add task(new/available) to project
"""

from solution.alltasks import AllTasks
from solution.task import Task
from solution.manager import Manager
from solution.logger import Logger
from solution.user import User
from solution.project import Project
from solution.status import STATUS
from solution.config import Config
from solution.priority import PRIORITIES
