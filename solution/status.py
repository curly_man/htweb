IN_PROGRESS = 'I'
COMPLETED = 'C'
FAIL = 'F'
PERIODIC = 'P'
STATUS = (
    (IN_PROGRESS, 'In progress'),
    (COMPLETED, 'Completed'),
    (FAIL, 'Fail'),
    (PERIODIC, 'Periodic'),
)
