import configparser
import os


class Config:
    def __init__(self, config_path=None):
        if not config_path:
            self.config_path = "config.ini"
        else:
            self.config_path = ".".join((config_path, "ini"))

    def create_config(self):
        config = configparser.ConfigParser()
        config.add_section("Files")
        config.set("Files", "data_folder", "data")
        config.set("Files", "manager", "manager.json")


        with open(self.config_path, "w") as config_file:
            config.write(config_file)

    def get_config(self):
        if not os.path.exists(self.config_path):
            self.create_config()

        config = configparser.ConfigParser()
        config.read(self.config_path)
        return config

    def get_setting(self, section, setting):
        config = self.get_config()
        return config.get(section, setting)