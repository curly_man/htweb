
import jsonpickle
import os


class Storage:
    def __init__(self):
        pass

    def write_manager(self, manager, folder, mngr):
        try:
            temp = jsonpickle.dumps(manager)
            with open(os.path.join(os.path.dirname(os.getcwd()), folder, mngr), 'w+') as file:
                file.write(temp)
        except:
            os.mkdir(os.path.dirname(os.getcwd()) + '\data')
            self.write_manager(manager, folder, mngr)

    def read_manager(self, folder, mngr):
        try:
            if os.path.exists(os.path.join(os.path.dirname(os.getcwd()), folder, mngr)):
                with open(os.path.join(os.path.dirname(os.getcwd()), folder, mngr), 'r+') as file:
                    manager = jsonpickle.loads(file.read())
                    return manager
        except:
            raise NameError('manager.JSON don\'t have information')