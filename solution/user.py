from .alltasks import AllTasks
from .status import STATUS
from .priority import PRIORITIES


class User:
    """
        login - user name
        tasks - user tasks
        projects - user projects
        tags - user tags
        id - unique id of a user
    """
    def __init__(self, login):
        self.login = login
        self.tasks = AllTasks()
        self.projects = {}
        self.tags = []
        self.id = hash(self)

    def check_finish(self):
        return self.tasks.check_finish()

    def add_task(self, task):
        self.tasks.add(task)

    def add_subtask(self, _id, task):
        self.tasks.add_sub(_id, task)

    def delete_task(self, _id):
        self.tasks.delete(_id)

    def complete_task(self, _id):
        self.tasks.complete(_id)

    def add_task_tag(self, tag, _id):
        if tag not in self.tags:
            self.tags.append(tag)
        self.tasks.add_tag(tag, _id)

    def delete_task_tag(self, tag, _id):
        self.tasks.delete_tag(tag, _id)

    def print_tags(self):
        if not self.tags:
            raise NameError('No tags')
        return self.tags

    def print_tasks(self):
        return self.tasks.print_tasks()

    def print_tasks_by_tag(self, tag):
        return self.tasks.print_by_tag(tag)

    def print_archived_tasks(self):
        return self.tasks.print_archive_tasks()

    def change_task(self, _id, name=None, status=None, priority=None, finish=None, description=None,
                    deadline=None, period=None):
        self.tasks.change_task(_id, name=name, status=status, priority=priority, finish=finish,
                               description=description, deadline=deadline, period=period)

    def get_task(self, _id):
        return self.tasks.get_task(_id)

    def add_project(self, project):
        self.projects[project.id] = project

    def get_projects(self):
        return self.projects

    def __str__(self):
        return self.login + ' id:' + str(self.id)
