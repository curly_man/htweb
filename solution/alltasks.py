from .status import STATUS
from .priority import PRIORITIES
from datetime import datetime, timezone


class AllTasks:
    def __init__(self):
        self.tasks = {}
        self.count = 0

    def add(self, task):
        """
            Function to add a new task to others.
            :param task: a new task to add
            :return: None
        """
        self.tasks[task.id] = task
        self.count = self.count + 1

    def delete(self, _id):
        """
            Delete a task with all its subtasks
            :param _id: task id of task to be removed
            :return: None
        """
        if _id in self.tasks:
            if self.tasks[_id].subtask:
                self.delete_subtasks(self.tasks[_id].subtask.tasks)
            del self.tasks[_id]
            self.count = self.count - 1
        else:
            raise NameError('Wrong id! Task dont deleted.')

    def delete_subtasks(self, tasks):
        for _id in tasks:
            self.delete(_id)

    def change_task(self, _id, name=None, status=None, priority=None, finish=None, description=None, deadline=None,
                    period=None):
        """
            Function to change a task
            :param _id: task id of task to be change
            :param name:  task new name
            :param status:  task new status
            :param priority:  task new priority
            :param finish:  task new finish
            :param description:  task new description
            :param deadline:  task new deadline
            :param period:  task new period
            :return: None
        """
        if _id not in self.tasks:
            raise NameError('Wrong id! You must input task id than you want to change')
        if name:
            self.tasks[_id].name = name
        if status:
            self.tasks[_id].status = STATUS[int(status)]
        if priority:
            self.tasks[_id].priority = PRIORITIES[(int(priority))]
        if deadline:
            self.tasks[_id].deadline = deadline
        if finish:
            self.tasks[_id].finish = finish
        if description:
            self.tasks[_id].description = description
        if period:
            self.tasks[_id].period = period

    def add_sub(self, _id, task):
        """
            Function to add a new task how subtask to task with _id.
            task: a new task to add
            :param _id: task id that be parent
            :param task: task than be subtask
            :return: None
        """
        if _id not in self.tasks:
            raise NameError('Wrong id! You must input task id than you want to add how subtask')
        if _id in self.tasks:
            task.parent = _id
            self.tasks[_id].subtask.add(task)
            self.add(task)
        else:
            raise NameError('Wrong id!')

    def complete(self, _id):
        """
            Function to complete a task.
            :param _id: id task to complete
            :return: None
        """
        if _id in self.tasks:
            if self.tasks[_id].period:
                self.start_task(_id)
                return
            self.tasks[_id].archived = True
            self.tasks[_id].status = STATUS[1]
            if self.tasks[_id].subtask:
                self.complete_subtask(self.tasks[_id].subtask.tasks)
        else:
            raise NameError('Wrong id!')

    def complete_subtask(self, tasks):
        for _id in tasks:
            self.complete(_id)

    def check_finish(self):
        """
            Check all tasks on fail
        """
        for _id in self.tasks:
            self.check_task_finish(self.tasks[_id])

    @staticmethod
    def check_task_finish(task):
        if task.finish:
            try:
                if datetime.now(timezone.utc) > task.finish:
                    if task.status != STATUS[1]:
                        task.status = STATUS[2]
                    elif task.period:
                        task.status = STATUS[1]
            except:
                raise NameError('')

    def start_task(self, _id):
        """
            For periodic task.
            :param _id: id task to start
            :return: None
        """
        if self.tasks[_id].deadline > self.tasks[_id].finish:
            self.tasks[_id].status = STATUS[1]
        else:
            self.tasks[_id].deadline = self.tasks[_id].deadline + self.tasks[_id].period
            while self.tasks[_id].deadline < datetime.now(timezone.utc):
                self.tasks[_id].deadline = self.tasks[_id].deadline + self.tasks[_id].period
                self.tasks[_id].status = STATUS[0]

    def add_tag(self, tag, _id):
        """
            Function to add tags.
            :param _id: id task to add tag
            :param tag: new task tag
            :return: None
        """
        try:
            self.tasks[_id].tags.append(tag)
        except:
            raise NameError('Wrong id! You must input task id than you want to add tag')

    def delete_tag(self, tag, _id):
        """
            Function to delete tags.
            :param _id: id task to delete tag
            :param tag: name removed tag
            :return: None
        """
        if _id in self.tasks:
            self.tasks[_id].tags.remove(tag)
        else:
            raise NameError('Wrong id! You must input task id than you want to delete tag')

    def get_tasks(self):
        """
        Function to give tasks
        :return: None
        """
        return self.tasks

    def get_task(self, _id):
        if _id in self.tasks:
            return self.tasks[_id]
        else:
            raise NameError('Wrong Task id.')

    def print_tasks(self):
        return self.tasks

    def print_by_tag(self, tag):
        tasks_tag = {}
        for _id in self.tasks:
            if tag in self.tasks[_id].tags:
                tasks_tag[self.tasks[_id]] = self.tasks[_id]
        return tasks_tag

    def print_archive_tasks(self):
        archived_tasks = {}
        for _id in self.tasks:
            if self.tasks[_id].archived:
                archived_tasks[_id] = self.tasks[_id]
                print(self.tasks[_id])
        return archived_tasks
