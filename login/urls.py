from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^login/', views.login),
    url(r'^registration/', views.registration, name='registration'),
    url(r'^logout/', views.logout, name='logout')
]