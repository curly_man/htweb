from django.shortcuts import render_to_response, redirect, render
from django.contrib import auth
from django.template.context_processors import csrf
from .models import UserCreateForm


def registration(request):
    form = UserCreateForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        new_user_form = UserCreateForm(request.POST)
        if new_user_form.is_valid():
            new_user_form.save()
            new_user = auth.authenticate(username=new_user_form.cleaned_data['username'],
                                         password=new_user_form.cleaned_data['password1'])
            auth.login(request, new_user)
            return redirect('/tracker/main')
    return render(request, 'registration.html', locals())


def login(request):
    context = {}
    context.update(csrf(request))
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)

        if user:
            auth.login(request, user)
            return redirect('/tracker/main')
        else:
            context['login_error'] = "Wrong"
            return render_to_response('login.html', context)
    else:
        return render_to_response('login.html', context)


def logout(request):
    auth.logout(request)
    return redirect('/auth/login')


