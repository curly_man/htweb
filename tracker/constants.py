LOW = 'L'
NORMAL = 'N'
HIGH = 'H'
PRIORITIES = (
    (LOW, 'Low'),
    (NORMAL, 'Normal'),
    (HIGH, 'High'),
)

IN_PROGRESS = 'I'
COMPLETED = 'C'
FAIL = 'F'
PERIODIC = 'P'
STATUS = (
    (IN_PROGRESS, 'In progress'),
    (COMPLETED, 'Completed'),
    (FAIL, 'Fail'),
    (PERIODIC, 'Periodic'),
)

DAYS_CHOICES = (
    (0, 'Mon'),
    (1, 'Tue'),
    (2, 'Wed'),
    (3, 'Thu'),
    (4, 'Fri'),
    (5, 'Sat'),
    (6, 'Sun')
)

INTERVAL_CHOICES = (
    ('h', 'hour(s)'),
    ('d', 'day(s)'),
    ('w', 'week(s)'),
    ('m', 'month(s)'),
    ('y', 'year(s)')
)