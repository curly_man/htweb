from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from dateutil import relativedelta
from multiselectfield import MultiSelectField
from .validate import validate_due, validate_interval
from .constants import (
    STATUS, IN_PROGRESS,
    PRIORITIES, NORMAL,
    DAYS_CHOICES
)
from solution.task import Task as LibTask


class Project(models.Model):
    name = models.CharField(max_length=40, default=None)
    users = models.ManyToManyField(User, related_name='project_users', default=None, null=True, blank=True)
    created_user = models.ForeignKey(User, related_name='created_user', on_delete=models.CASCADE, default=None,
                                     null=True, blank=True)

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=10, default=None, null=True)
    user = models.ForeignKey(User, default=None, null=True)

    # task = models.ManyToManyField('Task', null=True)

    def __str__(self):
        return self.name


class Notification(models.Model):
    user = models.ForeignKey(User, null=True, default=None)
    message = models.CharField(max_length=256)
    viewed = models.BooleanField(default=False)


class Task(models.Model):
    user = models.ForeignKey(User, null=True, default=None)
    tags = models.ManyToManyField(Tag, blank=True, default=None, null=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, default=None, blank=True, null=True)
    name = models.CharField(max_length=50, null=True)
    description = models.CharField(max_length=160, default=None, null=True)
    create_at = models.DateTimeField(default=datetime.now())
    deadline = models.DateTimeField(blank=True, default=None, null=True)
    end_time = models.DateTimeField(blank=True, null=True, default=None)
    status = models.CharField(max_length=1, choices=STATUS, default=IN_PROGRESS)
    priority = models.CharField(max_length=1, choices=PRIORITIES, default=NORMAL)
    parent = models.ForeignKey('self', null=True, blank=True, default=None, related_name='children')
    periodicity = models.CharField(max_length=30, blank=True, null=True, default=None, validators=[validate_interval])
    periodic = models.BooleanField(default=False)

    def __str__(self):
        return str(self.name)


class DjangoStorage:
    @staticmethod
    def save_task(task):
        try:
            new_task = Task.objects.get(id=task.id)
        except:
            new_task = Task()
            new_task.save()
        if new_task:
            new_task.name = task.name
            new_task.deadline = task.deadline
            new_task.description = task.description
            new_task.create_at = task.start
            new_task.end_time = task.finish
            new_task.periodicity = task.period
            if task.parent:
                new_task.parent = Task.objects.get(id=task.parent)
            print(task.tags)
            # new_task.tags = task.tags

            new_task.priority = task.priority
            new_task.status = task.status
            new_task.user = task.user
            new_task.save()

    @staticmethod
    def get_task(id):
        try:
            task = Task.objects.get(id=id)
            new_task = LibTask()
            if new_task:
                new_task.name = task.name
                new_task.deadline = task.deadline
                new_task.description = task.description
                new_task.start = task.create_at
                new_task.finish = task.end_time
                new_task.period = task.periodicity
                if task.parent:
                    new_task.parent = task.parent.id
                new_task.tags = Tag.objects.filter(user=task.user)
                new_task.priority = task.priority
                new_task.status = task.status
                new_task.user = task.user
                new_task.id = task.id
            return new_task
        except Exception as e:
            print(e)
