from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from datetime import datetime
import re


def validate_due(value):
    if datetime.now().strftime('%d-%m-%Y %H:%M') > datetime(value).strftime('%d-%m-%Y %H:%M'):
        raise ValidationError(
            _('%(value)s is in the past'),
            params={'value': value},
        )


def validate_interval(value):
    p = re.compile(r"((?:^[hdwmy]$)|(?:^[1-9][0-9]+[hdwmy]$))|(^daily$|^hourly$|^weekly$|^monthly$|^yearly$)")
    e = re.search(p, value)
    if not e:
        raise ValidationError(
            _('%(value)s is not right format'),
            params={'value': value},
        )