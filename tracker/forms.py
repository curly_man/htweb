from django import forms

from .models import Task, Tag


class AddTaskForm(forms.ModelForm):
    name = forms.CharField()
    deadline = forms.DateTimeField(input_formats=['%Y-%m-%d-%H-%M', '%Y-%m-%d %H:%M:%S'])
    end_time = forms.DateTimeField(input_formats=['%Y-%m-%d-%H-%M', '%Y-%m-%d %H:%M:%S'])
    description = forms.CharField(widget=forms.Textarea(attrs={'rows': 4, 'cols': 65}))
    tags = forms.CheckboxSelectMultiple(attrs={'height': 50})

    class Meta:
        model = Task
        fields = ['name', 'description', 'deadline', 'tags', 'priority',
                  'project', 'parent', 'end_time', 'periodicity']


class ChangeTaskForm(forms.ModelForm):
    name = forms.CharField()
    deadline = forms.DateField(input_formats=['%d %m %Y'])
    description = forms.CharField(widget=forms.Textarea(attrs={'rows': 4, 'cols': 65}))
    tags = forms.CheckboxSelectMultiple(attrs={'height': 50})

    class Meta:
        model = Task

        fields = ['name', 'description', 'deadline', 'tags', 'priority',
                  'project', 'parent', 'end_time', 'periodicity']


class AddTagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ['name']


class AddProjectForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ['name']
