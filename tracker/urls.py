from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^tag_details/(?P<id>\d+)/$', views.tag_details, name='tag_details'),
    url(r'^main/all_tasks', views.main_all_tasks, name='main_all_tasks'),
    url(r'^main/perform_tasks', views.main_perform_tasks, name='main_perform_tasks'),
    url(r'^delete_task/(?P<id>\d+)/$', views.delete_task, name='delete_task'),
    url(r'^main', views.main, name='main'),
    url(r'^details/(?P<id>\d+)/$', views.details, name='details'),
    url(r'^add_task_fast', views.add_task_fast, name='add_task_fast'),
    url(r'^add_task', views.add_task, name='add_task'),
    url(r'^project_users/(?P<id>\d+)/$', views.project_users, name='project_users'),
    url(r'^add_project', views.add_project, name='add_project'),
    url(r'^add_tag', views.add_tag, name='add_tag'),
    url(r'^tags', views.show_tags, name='tags'),
    url(r'^projects', views.show_projects, name='projects'),
    url(r'^projects', views.show_projects, name='projects'),
    url(r'^see_all_notifications', views.see_all_notifications, name='see_all_notifications'),
    url(r'^see_notification/(?P<id>\d+)/$', views.see_notification, name='see_notification'),

    url(r'^complete_task/(?P<id>\d+)/$', views.complete_task, name='complete_task'),
    url(r'^change_task/(?P<id>\d+)/$', views.change_task, name='change_task'),
    url(r'^tag_details/(?P<id>\d+)/$', views.tag_details, name='tag_details'),
    url(r'^project_details/(?P<id>\d+)/$', views.project_details, name='project_details')
]
