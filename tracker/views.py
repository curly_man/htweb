from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Task, Tag, Project, DjangoStorage, Notification
from django.contrib.auth.models import User
from django.contrib import auth
from .utils import get_time_delta
from django.utils import timezone
from .forms import (AddTaskForm, AddTagForm,
                    ChangeTaskForm, AddProjectForm)

from django.db.models import Q
from .constants import (
    COMPLETED, FAIL, IN_PROGRESS, PERIODIC
)
from notifications.signals import notify
from solution.manager import Manager, Task as LibTask


def details(request, id):
    task = Task.objects.get(id=id)
    tags = Tag.objects.filter(task=task)

    context = {
        'task': task,
        'tags': tags
    }
    check_task(request, task)
    return render(request, 'details.html', context)


def add_task(request):
    username = auth.get_user(request).username
    tags = Tag.objects.filter(user=request.user)
    projects = Project.objects.filter(users=request.user)
    context = {
        'username': username,
        'tags': tags,
        'projects': projects
    }
    if request.method == 'POST':
        form = AddTaskForm(request.POST)
        if form.is_valid():
            manager = Manager()
            manager.storage = DjangoStorage()
            task = Task()
            task.save()
            project = form.cleaned_data['project']
            if project:
                project = Project.objects.get(id=project.id)
            parent = form.cleaned_data['parent']
            if parent:
                parent = form.cleaned_data['parent'].id
            else:
                None
            tags = form.cleaned_data['tags']
            manager.add_user_task(id=task.id,
                                  name=form.cleaned_data['name'],
                                  description=form.cleaned_data['description'],
                                  deadline=form.cleaned_data['deadline'],
                                  finish=form.cleaned_data['end_time'],
                                  periodicity=form.cleaned_data['periodicity'],
                                  parent=parent,
                                  tags=tags,
                                  project=project,
                                  user=request.user)
            return redirect('/tracker/main')
    else:
        form = AddTaskForm()
    context['form'] = form
    return render(request, 'add_task.html', context)


def add_task_fast(request):
    if request.method == 'POST':
        name = request.POST['name']
        user = request.user
        task = Task(user=user, name=name)
        task.save()
        return redirect('/tracker/main')


def delete_task(request, id):
    task = Task.objects.get(id=id)
    print(task.id)
    if request.method == 'POST':
        subtasks = task.children.all()
        for task in subtasks:
            task.delete(keep_parents=True)
        task.delete()
    return redirect('/tracker/main')


# def complete_task(request, id):
#     task = Task.objects.get(id=id)
#     if request.method == 'POST':
#         check_task(task=task, comp=True)
#         task.save()
#     return redirect('/tracker/main')


def complete_task(request, id):
    task = Task.objects.get(id=id)
    if request.method == 'POST':
        manager = Manager()
        manager.storage = DjangoStorage()
        manager.complete_user_task(id)
        subtasks = task.children.all()
        for task in subtasks:
            task.delete(keep_parents=True)
    return main(request)  # redirect('/tracker/main')


def change_task(request, id):
    username = auth.get_user(request).username
    tags = Tag.objects.filter(user=request.user)
    task = Task.objects.get(id=id)
    tasks = Task.objects.all()
    context = {
        'username': username,
        'tags': tags,
        'task': task
    }
    if request.method == 'POST':
        form = AddTaskForm(request.POST)
        if form.is_valid():
            task.name = form.cleaned_data['name']
            task.description = form.cleaned_data['description']
            task.deadline = form.cleaned_data['deadline']
            task.end_time = form.cleaned_data['end_time']
            task.periodicity = form.cleaned_data['periodicity']
            if form.cleaned_data['parent']:
                task.parent = form.cleaned_data['parent'].id
            else:
                task.parent = None
            if task.deadline and task.periodicity:
                task.status = PERIODIC
                task.periodic = True
            else:
                task.periodic = False
                task.status = IN_PROGRESS

            task.save()
            task.tags = form.cleaned_data['tags']
            project = form.cleaned_data['project']
            if task.parent:
                task.parent = Task.objects.get(id=task.parent)
                task.save()
            if project:
                task.project = Project.objects.get(id=project.id)
            task.save()
            return redirect('/tracker/main')
    else:
        form = AddTaskForm(instance=task, initial={'status': task.status, 'name': task.name})
        check_tasks(request, tasks)
    context['form'] = form
    return render(request, 'change_task.html', context)


def add_tag(request):
    username = auth.get_user(request).username
    context = {
        'username': username
    }
    if request.method == 'POST':
        form = AddTagForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            user = request.user
            if not Tag.objects.filter(name=name).exists():
                tag = Tag(name=name, user=user)
                tag.save()
            return redirect('/tracker/tags')
    else:
        form = AddTagForm()
    context['form'] = form
    return render(request, 'add_tag.html', context)


def show_tags(request):
    tags = Tag.objects.filter(user=request.user)
    context = {
        'tags': tags
    }
    return render(request, 'tags.html', context)


@login_required(login_url='/auth/login')
def main(request):
    tasks = Task.objects.filter(
        Q(status__in=[IN_PROGRESS, PERIODIC, 'In progress']) & (Q(user=request.user) & Q(project=None)))
    check_tasks(request, tasks)
    tasks = Task.objects.filter(
        Q(status__in=[IN_PROGRESS, PERIODIC, 'In progress']) & (Q(project__in=request.user.project_users.all())
                                                                | (Q(user=request.user) & Q(project=None))))

    username = auth.get_user(request).username
    notifications = Notification.objects.filter(user=request.user, viewed=False)
    print(timezone.now())
    context = {
        'tasks': tasks,
        'username': username,
        'notifications': notifications
    }

    return render(request, 'main.html', context)


def main_all_tasks(request):
    tasks = Task.objects.filter((Q(project__in=request.user.project_users.all())
                                 | (Q(user=request.user) & Q(project=None))))
    username = auth.get_user(request).username
    context = {
        'tasks': tasks,
        'username': username
    }
    check_tasks(request, tasks)
    return render(request, 'main.html', context)


def main_perform_tasks(request):
    tasks = Task.objects.filter(Q(user=request.user) & Q(status__in=[COMPLETED, PERIODIC]))
    username = auth.get_user(request).username
    context = {
        'tasks': tasks,
        'username': username
    }
    check_tasks(request, tasks)
    return render(request, 'main.html', context)


def tag_details(request, id):
    username = auth.get_user(request).username
    tag = Tag.objects.get(id=id)
    tasks = tag.task_set.all()
    tags = Tag.objects.filter(user=request.user)

    context = {
        'tasks': tasks,
        'username': username,
        'tags': tags,
        'tag': tag
    }
    check_tasks(request, tasks)
    return render(request, 'tag_details.html', context)


def add_project(request):
    username = auth.get_user(request).username
    context = {
        'username': username
    }
    if request.method == 'POST':
        name = request.POST['name']
        user = request.user
        project = Project(name=name, created_user=user)
        project.save()
        project.users.add(user)
        project.save()
        return redirect('/tracker/projects')
    else:
        form = AddTagForm()
    context['form'] = form
    return render(request, 'add_project.html', context)


def show_projects(request):
    projects = Project.objects.filter(users=request.user)
    context = {
        'projects': projects
    }
    return render(request, 'projects.html', context)


def project_details(request, id):
    username = auth.get_user(request).username
    project = Project.objects.get(id=id)
    owner = project.created_user.username
    tasks = Task.objects.filter(project=project.id)
    users = project.users.all()
    tags = Tag.objects.filter(user=request.user)
    projects = Project.objects.filter(users=request.user)
    context = {
        'tasks': tasks,
        'users': users,
        'username': username,
        'tags': tags,
        'projects': projects,
        'project': project,
        'owner': owner
    }
    check_tasks(request, tasks)
    return render(request, 'project_details.html', context)


def project_users(request, id):
    project = Project.objects.get(id=id)
    username = auth.get_user(request).username
    tasks = Task.objects.filter(status__in=[IN_PROGRESS, FAIL, PERIODIC], user=request.user)
    tags = Tag.objects.filter(user=request.user)
    projects = Project.objects.filter(users=request.user)
    users = User.objects.exclude(id=project.created_user.id)
    context = {
        'tasks': tasks,
        'username': username,
        'tags': tags,
        'projects': projects,
        'users': users,
        'project': project,
    }
    check_tasks(request, tasks)
    if request.method == 'POST':
        project.users = User.objects.filter(id__in=request.POST.getlist('user'))
        project.users.add(request.user)
        project.save()
        return redirect('/tracker/projects')
    return render(request, 'project_users.html', context)


def check_tasks(request, tasks):
    for task in tasks:
        check_task(request, task)


def check_task(request, task, comp=False):
    if task.periodic:
        check_periodic_task(request, task, comp)
    else:
        if task.deadline:
            check_standard_task(request, task, comp)
        elif comp:
            task.status = COMPLETED
            task.save()


def check_periodic_task(request, task, comp=None):
    if task.deadline > task.end_time:
        task.status = COMPLETED
        task.save()
    else:
        if not comp:
            start_task(task)


def check_standard_task(request, task, comp):
    if task.deadline <= timezone.now():
        task.status = FAIL
        notification = Notification(user=request.user, message='Task {0} Fail'.format(task.name))
        notification.save()
    elif comp:
        task.status = COMPLETED
        subtasks = task.children.all()
        for task in subtasks:
            task.delete(keep_parents=True)
    task.save()


def start_task(task):
    if task.periodicity:
        parsed_interval = get_time_delta(task.periodicity)
        task.deadline = task.deadline + parsed_interval
        while task.deadline < timezone.now():
            task.deadline = task.deadline + parsed_interval
        if task.periodic:
            task.status = PERIODIC
        else:
            task.status = IN_PROGRESS
        task.save()


def see_all_notifications(request):
    notifications = Notification.objects.filter(user=request.user)
    for notification in notifications:
        notification.viewed = True
        notification.save()
    return redirect('/tracker/main')


def see_notification(request, id):
    notification = Notification.objects.get(id=id)
    notification.viewed = True
    notification.save()
    return redirect('/tracker/main')
